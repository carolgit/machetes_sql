CREATE TABLE IF NOT EXISTS metadados.dicionario_colunas
(
    id_pk integer NOT NULL,
    coluna character varying COLLATE pg_catalog."default" NOT NULL,
    ano integer NOT NULL,
    o_que character varying COLLATE pg_catalog."default" NOT NULL,
    porque character varying COLLATE pg_catalog."default" NOT NULL,
    como character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT dicionario_colunas_pkey PRIMARY KEY (id_pk)
);
CREATE TABLE IF NOT EXISTS metadados.dicionario_tabelas
(
    id_pk integer NOT NULL,
    ativo boolean NOT NULL DEFAULT true,
    esquema character varying COLLATE pg_catalog."default" NOT NULL,
    tabela character varying COLLATE pg_catalog."default" NOT NULL,
    ano integer NOT NULL,
    o_que text COLLATE pg_catalog."default" NOT NULL,
    porque text COLLATE pg_catalog."default" NOT NULL,
    como text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT dicionario_tabelas_pkey PRIMARY KEY (id_pk)
);
CREATE SEQUENCE metadados.dicionario_colunas_seq;
CREATE SEQUENCE metadados.dicionario_tabelas_seq;
CREATE SCHEMA topologia;
CREATE TABLE IF NOT EXISTS topologia.bacia
(
    id_pk integer NOT NULL,
    nome character varying COLLATE pg_catalog."default" NOT NULL,
    id_subbacia integer NOT NULL,
    fonte character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT bacia_pkey PRIMARY KEY (id_pk),
    CONSTRAINT fk_subbacia FOREIGN KEY (id_subbacia)
        REFERENCES topologia.bacia (id_pk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);
CREATE TABLE IF NOT EXISTS topologia.pais
(
    id_pk integer NOT NULL,
    nome character varying COLLATE pg_catalog."default" NOT NULL,
    capital character varying COLLATE pg_catalog."default" NOT NULL,
    extensao integer NOT NULL,
    idioma character varying COLLATE pg_catalog."default" NOT NULL,
    localizacao character varying COLLATE pg_catalog."default" NOT NULL,
    moeda character varying COLLATE pg_catalog."default" NOT NULL,
    sigla character varying(2) COLLATE pg_catalog."default",
    CONSTRAINT pais_pkey PRIMARY KEY (id_pk)
);
CREATE TABLE IF NOT EXISTS topologia.braco
(
    id_pk integer NOT NULL,
    nome character varying COLLATE pg_catalog."default" NOT NULL,
    id_pais integer NOT NULL,
    CONSTRAINT braco_pkey PRIMARY KEY (id_pk),
    CONSTRAINT fk_pais FOREIGN KEY (id_pais)
        REFERENCES topologia.pais (id_pk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
CREATE TABLE IF NOT EXISTS topologia.estado
(
    id_pk integer NOT NULL,
    nome character varying COLLATE pg_catalog."default" NOT NULL,
    id_pais integer NOT NULL,
    regiao character varying COLLATE pg_catalog."default" NOT NULL,
    sigla character varying(2) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT estado_pkey PRIMARY KEY (id_pk),
    CONSTRAINT fk_pais FOREIGN KEY (id_pais)
        REFERENCES topologia.pais (id_pk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
CREATE TABLE IF NOT EXISTS topologia.municipio
(
    id_pk integer NOT NULL,
    nome character varying COLLATE pg_catalog."default" NOT NULL,
    id_estado integer NOT NULL,
    CONSTRAINT municipio_pkey PRIMARY KEY (id_pk),
    CONSTRAINT fk_estado FOREIGN KEY (id_estado)
        REFERENCES topologia.estado (id_pk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);
CREATE TABLE IF NOT EXISTS topologia.v_bacia
(
    id_pk integer NOT NULL,
    id_bacia integer NOT NULL,
    the_geom polygon[] NOT NULL,
    CONSTRAINT v_bacia_pkey PRIMARY KEY (id_pk),
    CONSTRAINT fk_bacia FOREIGN KEY (id_bacia)
        REFERENCES topologia.bacia (id_pk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
CREATE TABLE IF NOT EXISTS topologia.v_braco
(
    id_pk integer NOT NULL,
    the_geom polygon[] NOT NULL,
    id_braco integer NOT NULL,
    CONSTRAINT v_braco_pkey PRIMARY KEY (id_pk),
    CONSTRAINT fk_braco FOREIGN KEY (id_braco)
        REFERENCES topologia.braco (id_pk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
CREATE TABLE IF NOT EXISTS topologia.v_estado
(
    id_pk integer NOT NULL,
    id_estado integer NOT NULL,
    the_geom polygon[] NOT NULL,
    CONSTRAINT v_estado_pkey PRIMARY KEY (id_pk),
    CONSTRAINT fk_estado FOREIGN KEY (id_estado)
        REFERENCES topologia.estado (id_pk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
CREATE TABLE IF NOT EXISTS topologia.v_municipio
(
    id_pk integer NOT NULL,
    the_geom polygon[] NOT NULL,
    id_municipio integer NOT NULL,
    CONSTRAINT v_municipio_pkey PRIMARY KEY (id_pk),
    CONSTRAINT fk_municipio FOREIGN KEY (id_municipio)
        REFERENCES topologia.municipio (id_pk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
CREATE TABLE IF NOT EXISTS topologia.v_pais
(
    id_pk integer NOT NULL,
    the_geom polygon[] NOT NULL,
    id_pais integer,
    CONSTRAINT v_pais_pkey PRIMARY KEY (id_pk),
    CONSTRAINT fk_pais FOREIGN KEY (id_pais)
        REFERENCES topologia.pais (id_pk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

