--CRIANDO EXEMPLO DE TRIGGERS PARA PREENCHER TABELA DE AUDITORIA
--PASSO 1 CRIAR TABELA DE AUDITORIA
CREATE SCHEMA metadados;
CREATE TABLE IF NOT EXISTS metadados.auditoria
(
	id_pk integer NOT NULL,
	usuario_autenticado character varying COLLATE pg_catalog."default" NOT NULL,
	acao character varying COLLATE pg_catalog."default" NOT NULL,
	comando_sql character varying COLLATE pg_catalog."default" NOT NULL,
	data_hora timestamp with time zone NOT NULL,
	CONSTRAINT auditoria_pkey PRIMARY KEY (id_pk)
);
CREATE SEQUENCE metadados.auditoria_seq;
--PASSO 2 criar funcao que ao ter algum evento dll(create, alter, drop) insira uma linha em auditoria
CREATE OR REPLACE FUNCTION on_change_audit_func()
RETURNS event_trigger AS $$
	BEGIN
	    -- tg_tag vem do evento que dispara essa event trigger
		INSERT INTO metadados.auditoria(id_pk, usuario_autenticado, acao, comando_sql, data_hora)
	    VALUES(nextval('metadados.auditoria_seq'), current_user, tg_tag, current_query(), NOW());
	END
$$
LANGUAGE plpgsql;

--terminou de crear, remover e etc ele dispara esse evento chamando a funcao
CREATE EVENT TRIGGER on_change_audit ON ddl_command_end
EXECUTE FUNCTION on_change_audit_func();

CREATE OR REPLACE FUNCTION log_crud_changes()
RETURNS trigger AS $$
	BEGIN
		-- tg_op vem da trigger usada para operacoes de insert, delete, update
		INSERT INTO metadados.auditoria(id_pk, usuario_autenticado, acao, comando_sql, data_hora)
	    VALUES(nextval('metadados.auditoria_seq'), current_user, tg_op, current_query(), NOW());
		RETURN coalesce(NEW,OLD);
	END
	$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION on_create_audit_func()
RETURNS event_trigger AS $$
DECLARE
	obj record;
	table_name text;
	new_trigger_name text;
	schema_name text;
	new_trigger_name_insert text;
	new_trigger_name_delete text;
	new_trigger_name_update text;
BEGIN
	-- quando o evento for criar tabela criar trigger que observa a tabela e preenche auditoria 
	FOR obj IN SELECT * FROM pg_event_trigger_ddl_commands() WHERE command_tag = 'CREATE TABLE'
	LOOP
		table_name := split_part(obj.objid::regclass::TEXT, '.'::TEXT, 2);
	    new_trigger_name := table_name || '_auditoria_trigger';
		new_trigger_name_insert := new_trigger_name||'_insert';
		new_trigger_name_update := new_trigger_name||'_update';
		new_trigger_name_delete := new_trigger_name||'_delete';
	    schema_name := obj.schema_name||'.'||table_name;

	    EXECUTE format('CREATE TRIGGER %s AFTER INSERT ON %s
	    FOR EACH ROW
	    EXECUTE FUNCTION log_crud_changes()', new_trigger_name_insert, schema_name);

		EXECUTE format('CREATE TRIGGER %s AFTER DELETE ON %s
	    FOR EACH ROW
	    EXECUTE FUNCTION log_crud_changes()', new_trigger_name_update, schema_name);

		EXECUTE format('CREATE TRIGGER %s AFTER UPDATE ON %s
	    FOR EACH ROW
	    EXECUTE FUNCTION log_crud_changes()', new_trigger_name_delete, schema_name);
	END LOOP;
END;
$$
LANGUAGE plpgsql;

CREATE EVENT TRIGGER
on_create_audit ON ddl_command_end
EXECUTE FUNCTION on_create_audit_func();

-- CRIAR TABELA EXEMPLO PARA TESTE
CREATE TABLE teste(id_pk integer primary key, description text);
INSERT INTO teste(id_pk,description)VALUES(1,'testando');
-- VER SE FUNCIONOU
SELECT * FROM metadados.auditoria;